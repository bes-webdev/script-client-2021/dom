/*
    Récupérez l'élément ayant pour ID main-content grâce à son ID ;

    Récupérez les éléments ayant pour classe  important  ;

    Récupérez les éléments de type  article  ;

    Récupérez les éléments de type  li qui sont dans une liste ul ayant la classe  important . Cette liste doit elle-même être dans un article (article ) ;

    Envoyez dans la console le contenu html du deuxième élément du résultat de la sélection precédente.

 */


let main = document.getElementById("main-content");
console.log(main);

let importantList = document.getElementsByClassName('important');
console.log(importantList);

let articles = document.getElementsByTagName('article');
console.log(articles);

let querySelecytorResult = document.querySelectorAll('article ul.important > li');
console.log(querySelecytorResult);
console.log(querySelecytorResult[1].innerHTML);



