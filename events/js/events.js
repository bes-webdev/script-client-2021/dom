let clickNumber = 0;

// document.getElementById('myButton')
document.getElementById('link')
    .addEventListener('click', buttonClicked)


function buttonClicked(event) { // event est un argument fourni par js qui est un object représentant un évènement (celui qui vient de se déclencher) voir ici https://developer.mozilla.org/fr/docs/Web/API/Event
    console.log(event);
    event.preventDefault();
    clickNumber ++;
    document.querySelector('#monitor span').textContent = clickNumber;
}


document.querySelectorAll('.buttonList')// A ce niveau j'ai une liste d'élément sous form d'Iterable (un genre d'array) (la méthode dsaddEventListener n''est pas disponible sur un ArrayList)
    .forEach(function(element) { // Une manière de boucler sur un tableau ou un iterable, la fonction de rappel prend en argument l'element courant de la boucle.
        element.addEventListener('click', listElementClicked)
    });


function listElementClicked(event) {
    console.log('listElementClicked');
    console.log(event.target);
    console.log(event.currentTarget);
    // let target = event.target; // target est une propriété de event et qui contient l'element du DOM qui a déclenché l'évènement.
    let button = event.currentTarget; // currentTarget est une propriété de event et qui contient l'element du DOM qui a écoutait l'évènement.
    // textContent retourne un string, si je l'utilise directement avec un + ça va concaténer à la place d'additionner
    // On met donc le string dans la funciton aprseInt qui va transformer un string en number.
    button.querySelector('span').textContent = parseInt(button.querySelector('span').textContent, 10) + 1;
}

