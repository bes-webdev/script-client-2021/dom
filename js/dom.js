let title = document.getElementById('mainTitle');
if (null !== title) {
    title.innerHTML = "Nooooon je dirai pas bonjour !";
    console.log('Node type : ' +typeof title);
    console.log(title);
}

// retourne un tableau
let lis = document.getElementsByClassName('content');
for(let i = 0; i < lis.length; i ++) {
    lis[i].innerHTML += i;
    lis[i].style = "color: red;";
    lis[i].className += " class2";

}

// Modifier le dom
let modifyElement = document.getElementById('modify');
modifyElement.innerHTML = '<h1>Ce contenu a été <span>généré</span> par le js</h1>';
// textContent retourne le contenu d'un element sans les balises
console.log(modifyElement.textContent);
console.log(modifyElement.innerHTML);

// Les attributs class
modifyElement.className = "section mt-5";
let classesAsArray = modifyElement.className.split(' '); // L'inverse de split est join('separator')
console.log(classesAsArray);
let classList = modifyElement.classList;
console.log(classList);
classList.toggle('section'); // Si il a la class section il la retire, sinon il l'ajoute
console.log(modifyElement.className);
classList.toggle('section'); // Si il a la class section il la retire, sinon il l'ajoute
console.log(modifyElement.className);
classList.replace('section', 'hyperGreen');

// Style -> les regles css
let h1 = modifyElement.children[0]; // retourne le premietr element de mon element <section>
h1.style.color = '#666';
h1.style.fontSize = '2.5rem';
document.getElementsByTagName('body')[0].style.fontFamily = "arial, sans";

// Ajouter des éléments
// Je crée un nouvel element qui ne sera pas en,core présent dans mle dom après sa création
const aElement = document.createElement('a');
aElement.href = "https://framagit.com";
aElement.textContent = "Framagit";
// aElement est un élement stocké ici mais pas encore présent dans l'arborescence
modifyElement.appendChild(aElement);
